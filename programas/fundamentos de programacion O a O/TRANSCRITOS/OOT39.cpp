#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>

class Expense
{
      public:
             char TripName[80], Date[80];
             int Mileage, Cost, Fringe, Executive;
             void AskQuestions(void);
             void CheckWithBoss(void);
};

class ExecutiveExp: public Expense
{
      public:
             int Rating;
             virtual void AskQuestions(void);
             virtual void CheckWithBoss(void);
};

void Expense::AskQuestions(void)
{
     printf("Introduzca el nombre de su viaje: ");
     scanf("%s", TripName);
     printf("\n");
     printf("Fecha de su partida (mm/dd/aa)?");
     scanf("%s", Date);
     printf("\n");
     printf("Introduzca cantidad de millas del viaje: ");
     scanf("%d", &Mileage);
     printf("\n");
     printf("Introduzca costo del viaje: ");
     scanf("%d", &Cost);
     printf("\n");
     printf("Introduzca costo de comidas y entretenimiento: ");
     scanf("%d", &Fringe);
     printf("\n");
}

void Expense::CheckWithBoss(void)
{
     if(!stricmp(TripName, "Ixtapa")||!stricmp(TripName, "Cancun"))
        printf("%s: Esta despedido por pasarse de listo\n", TripName);
     else if (Cost/4 < Fringe)
        printf("Los gastos de entretenimiento %s estan muy altos\n", TripName);
     else
        printf("Viaje a %s aprobado\n", TripName);
}

void ExecutiveExp::AskQuestions(void)
{
     Expense::AskQuestions();
     printf("Como califica el viaje (0-10)?");
     scanf("%d", &Rating);
}

void ExecutiveExp::CheckWithBoss(void)
{
     if(Cost/2<Fringe)
        printf("¡Cuidado. No se esta divirtiendo mucho!\n");
}

Expense *ExRec[20];
int ExCount, I;
unsigned char Executive;

int main(int, char *)
{
    printf("GONZALEZ REYES ERIK JHORDAN\n");
    printf("Es usted ejecutivo? (y/n)");
    printf("\n");
    if(toupper(getch())=='y')
       Executive=1;
    else
       Executive=0;
    printf("Cuantos viajes hizo usted? ");
    scanf("%d", &ExCount);
    printf("\n");
    for(I=0; I<ExCount; I++)
         if(Executive==1)
            ExRec[I] = new ExecutiveExp;
         else
            ExRec[I] = new Expense;
    for(I=0; I<ExCount; I++)
       ExRec[I]->AskQuestions();
    for(I=0; I<ExCount; I++)
       ExRec[I]->CheckWithBoss();
    
    getch();
}

#include <stdio.h>
#include <conio.h>

class base1
{
      protected:
                int x;
                
      public:
             void showx()
             {
                  printf("x\n");
             };
};

class base2
{
      protected: 
                int y;
      
      public:
             void showy()
             {
                  printf("y\n");
             };
};

class derivada: public base1, public base2
{
      public:
             void set(int i, int j)
             {
                  x=i; 
                  y=j;
             };
};

int main()
{
    derivada obj;
    
          printf("GONZALEZ REYES ERIK JHORDAN\n");
    obj.set(10,20);
    obj.showx();
    obj.showy();
    getch();
}

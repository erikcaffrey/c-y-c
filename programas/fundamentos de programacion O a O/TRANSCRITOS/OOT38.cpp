#include <stdio.h>
#include <conio.h>
#include <string.h>

class Expense
{
      public:
             char TripName[80], Date[80];
             int Mileage, Cost, Fringe;
             void AskQuestions(void);
             void CheckWithBoss(void);
};

Expense ExRec[20];
int ExCount, I;

void Expense::AskQuestions(void)
{
     printf("Introduzca el nombre de su viaje: ");
     scanf("%s", TripName);
     printf("\n");
     printf("Fecha de su partida (mm/dd/aa)?");
     scanf("%s", Date);
     printf("\n");
     printf("Introduzca cantidad de millas del viaje: ");
     scanf("%d", &Mileage);
     printf("\n");
     printf("Introduzca costo del viaje: ");
     scanf("%d", &Cost);
     printf("\n");
     printf("Introduzca costo de comidas y entretenimiento: ");
     scanf("%d", &Fringe);
     printf("\n");
}

void Expense::CheckWithBoss(void)
{
     if(!stricmp(TripName, "Ixtapa")|| !stricmp(TripName, "Cancun"))
        printf("%s: Esta despedido por pasarse de listo\n", TripName);
     else if (Cost/4 < Fringe)
        printf("Los gastos de entretenimiento %s estan muy altos\n", TripName);
     else
        printf("Viaje a %s aprobado\n", TripName);
}

int main(int, char *)
{
    printf("GONZALEZ REYES ERIK JHORDAN\n");
    printf("Cuantos viajes hizo usted? ");
    scanf("%d", &ExCount);
    printf("\n");
    for(I=0; I<ExCount; I++)
       ExRec[I].AskQuestions();
    for(I=0; I<ExCount; I++)
       ExRec[I].CheckWithBoss();
    
    getch();
}

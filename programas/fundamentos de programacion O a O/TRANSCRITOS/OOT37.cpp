#include <stdio.h>
#include <conio.h>
#include <math.h>

float pi=3.141516;

class calcular
{     
      public:
             float radio, area, longitud;
             void iniciar(void);
             void entradaDatos(void);
             void salidaDatos(void);
} calcular1;

void calcular::iniciar(void)
{
              printf("GONZALEZ REYES ERIK JHORDAN\n");
     printf("Programa que calcula el area y longitud de una circunferencia\n\n");
}

void calcular::entradaDatos(void)
{
     printf("Introduzca el Radio de la Circunferencia: \n");
     scanf("%f",&radio);
     
     area=pi*(radio*radio);
     longitud=2*pi*radio;
}

void calcular::salidaDatos(void)
{
     printf("Area = %f\n", area);
     printf("Longitud = %f\n", longitud);
}

calcular calcular2;
int main()
{
    calcular1.iniciar();
    calcular2.iniciar();
    
    calcular1.entradaDatos();
    calcular2.entradaDatos();
    
    calcular1.salidaDatos();
    calcular2.salidaDatos();
    getch();
}


     

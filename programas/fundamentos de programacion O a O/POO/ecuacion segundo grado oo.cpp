//ecuacion de segundo grado
//GONZALES REYES ERIK JHORDAN
#include<stdio.h>
#include<conio.h>
#include<math.h>

class algebra
{
      public:
      float a,b,c,aux;
      
      void ingresar(void);
      void evaluar(void);
      void ecuacion(void);
      
};

 void algebra:: ingresar(void)
{      
        printf("\n\n==========ECUACION DE SEGUNDO GRADO============");
        printf("\n\n ingresa valor de a:");
        scanf("%f",&a);
        printf("\n\n ingresa valor de b:");
        scanf("%f",&b);
        printf("\n\n ingresa valor de c:");
        scanf("%f",&c);
        
}     
 void algebra:: evaluar(void)
{   
    aux=b*b-4*a*c;
    if(a>0 && aux > 0)
    {
     printf("\n\n Resultados de ecuacion");
     ecuacion();
     }  
     else
     {
       printf("\n\n ERROR a ES MENOR A CERO O RAIZ NEGATIVA");   
     }
}
void algebra:: ecuacion(void)
{ 
   double x1,x2;
   x1 = (((-1 *b) + sqrt((aux)))/(2*a));
   x2 = (((-1 *b) - sqrt((aux)))/(2*a));
   printf("\nX1:%.1f",x1);
   printf("\nX2:%.1f",x2);
}
 


algebra dosgrado;

main()
{     
      printf("\n\n GONZALEZ REYES ERIK JHORDAN");
      dosgrado.ingresar();
      dosgrado.evaluar();
             
       getch();
       return 0;
}


#include<stdio.h>
#include <conio.h>
 
int main() {
  
   printf( "Ejemplo de \"kbhit\"\r\n\r\n" );
   printf( "El programa est&aacute; a la espera de registrar una tecla pulsada.\r\n" );
   while( !kbhit() );
   printf( "El car&aacute;cter es \'%c\'\r\n", getch() );
   printf( "Pulsa una tecla para continuar...\r\n" );
   getch();
 
   return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <math.h>

char signos[4];
char infija[10][2]={"x","+","t","*","w","/","k","^","y"};
char postfija[10];
char operandos[5];
void pushope(char);
void pushsig(char);
void pushpos(char);
void conver();
int evaluar();
void imprimiendoPosfija();
void imprimiendoOperadores();
void imprimiendoOperandos();


int po=0;
int i=0;
int ps=0;
int pp=0;
int j;
main()
{  
   int opc;
   printf("\nPrograma que convierte un expresion postfija a infija");
   printf("\n\nSe convertira la siguiente expresion: x+t*w/k-y\n");
   printf("\n\nMenu");
   printf("\n1.-Convetir");
   printf("\n2.-Evaluarla");
   printf("\n3.-Salir");
   printf("\n\nEleja la Opcion Deseada:");
   scanf("%d",&opc);
   switch(opc)
   {
   case 1:
   conver();
   imprimiendoOperandos();
   imprimiendoOperadores();
   imprimiendoPosfija();
   break;
   case 2:
          printf("\n\nLa expresion infija= x+t*w/k^y");
          printf("\nLa expresion posfija= xtw*ky^/+");
          printf("\n\nEl resultado es: %d ",evaluar());
          evaluar();
   break;
   case 3:
         return(0);
   break;
   default:
          printf("\n\nOpcion Invalidad");
   break;
   }
   getch();
}

void pushope(char aux)
{
  if(po==5)
  {
    printf("\nPila llena");
  }
  else
  {
   operandos[po]=aux;
   po=po+1;
   
  }
}
void pushsig(char aux)
{
  if(ps==4)
  {
    printf("\nPila llena");
  }
  else
  {
   signos[ps]=aux;
   ps=ps+1; 
  }
}
void imprimiendoPosfija ()
{
   printf("\n\n\tExpresion postfija");  
   for(j=4;j>=0;j--)
   {
     printf("\n\t%c",operandos[j]);
   }
   for(j=3;j>=0;j--)
   {
     printf("\n\t%c",signos[j]);
   }
}
 void imprimiendoOperadores()
{
   printf("\n\n\tPila Operadores\n");
   for(j=3;j>=0;j--)
   {
     printf("\n\t%c",signos[j]);
   }
}
void imprimiendoOperandos()
{
  printf("\n\n\tPila Operandos");
  for(j=4;j>=0;j--)
  {
    printf("\n\t%c",operandos[j]);
  }
}
void conver()
{
  
 char aux;
 char auxs;
 printf("\n\n\tExpresion Infija");
 for( i=0;i<9;i++)
   {
     printf("\n\t%s",&infija[i]);
     if(infija[i][0]=='x' ||infija[i][0]=='t' || infija[i][0]=='w' ||infija[i][0]=='k' ||infija[i][0]=='y')
     {
       aux=infija[i][0];
       pushope(aux);
     }
     else if(infija[i][0]=='+' ||infija[i][0]=='*' || infija[i][0]=='/'|| infija[i][0]=='^')
     {
       aux=infija[i][0];
       pushsig(aux);
     }
   }
}
int evaluar()
{
  double final=0, x=3, t=5, w=7,k=9 , y=11, potencia=0;
  potencia=pow(k,y);
  final=x+(t*w)/potencia;
  return final;
}
